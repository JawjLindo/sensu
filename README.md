# Sensu Server for Testing

## Requirements
- [Docker](https://docs.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Usage
```
https://JawjLindo@bitbucket.org/JawjLindo/sensu.git
cd sensu
docker-compose up
```

## Notes
The ```sensu-conf.d``` directory is a mapped volume into the ```/etc/sensu/conf.d``` directory inside the sensu container. You can add checks into this directory and then restart the container to load the changes.

A ```check_cron.json``` check is included as a sample.
