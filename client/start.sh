#!/bin/bash

i="0"

while [ $i -lt 30 ]
do
if [ $(ps -ef | grep -v grep | grep sensu-client | wc -l) -gt 0 ]; then
  break
else
  echo "Attemping to start sensu-client..."
  /etc/init.d/sensu-client start
fi
sleep 3s
i=$[$i+1]
done

node src/index.js
