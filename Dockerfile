FROM ubuntu:15.04

RUN apt-get install -y wget

RUN wget -q http://repos.sensuapp.org/apt/pubkey.gpg -O- | apt-key add -
RUN echo "deb     http://repos.sensuapp.org/apt sensu main" | tee /etc/apt/sources.list.d/sensu.list

RUN apt-get update -y
RUN apt-get install -y sensu

COPY ./config.json /etc/sensu/config.json
VOLUME /etc/sensu/conf.d
RUN chown -R sensu:sensu /etc/sensu

EXPOSE 4567

CMD service sensu-server start && service sensu-api start && tail -f /dev/null
